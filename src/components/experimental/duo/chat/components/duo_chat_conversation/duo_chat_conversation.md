A component that lists messages in a conversation, and presents an optional delimiter to
mark the beginning of the conversation.

## Usage

```html
<gl-duo-chat-conversation :messages="messages" :show-delimeter="showDelimiter" />
```
